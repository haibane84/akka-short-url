package com.jeado.shorurl

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, MessageEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.concurrent.ScalaFutures
import akka.http.scaladsl.unmarshalling.Unmarshaller._
import com.jeado.shorurl.ShortUrlServiceActor.ShortUrl

class ShortUrlV1RoutesSpec extends FlatSpec
  with Matchers
  with ScalaFutures
  with ScalatestRouteTest
  with ShortUrlV1Routes {

  import PlayJsonSupport._
  val shortUrlServiceActor = system.actorOf(ShortUrlServiceActor.props)

  "ShortUrlV1Routes" should "create hash for given url (GET /v1/shorturl)" in {
    val reqEntity = Marshal(CreateShortUrl("http://google.com")).to[MessageEntity].futureValue
    val createReq = Post("/v1/shorturl").withEntity(reqEntity)

    createReq ~> routes ~> check {
      status shouldBe StatusCodes.OK
      contentType shouldBe ContentTypes.`application/json`

      entityAs[String] should include("token")
    }
  }

  it should "redirect to original url by given token (POST /v1/shorturl)" in {
    val url = "http://google.com"
    val reqEntity = Marshal(CreateShortUrl(url)).to[MessageEntity].futureValue
    val createReq = Post("/v1/shorturl").withEntity(reqEntity)
    var token = ""

    createReq ~> routes ~> check {
      status shouldBe StatusCodes.OK
      contentType shouldBe ContentTypes.`application/json`

      token = entityAs[ShortUrl].token
    }

    val redirectReq = HttpRequest(uri = "/"+token)

    redirectReq ~> routes ~> check {
      status shouldBe StatusCodes.PermanentRedirect
      header("Location").get shouldBe Location(url)
    }

  }
}
