package com.jeado.shorurl

import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import org.scalatest.{FlatSpecLike, Matchers}

import scala.concurrent.duration._

class ShortUrlServiceActorSpec
  extends TestKit(ActorSystem("ShortUrlServiceActorSpec"))
  with Matchers
  with ImplicitSender
  with FlatSpecLike {

  import ShortUrlServiceActor._

  val shortUrlService = system.actorOf(ShortUrlServiceActor.props)

  "ShortUrlServiceActor" should "reply ShortUrl when it got GetShortUrlByUrl" in {
    val url = "http://foo.com"
    shortUrlService ! GetShortUrlByUrl(url)
    expectMsgType[ShortUrl]
  }

  it should "provide different token from different url" in {
    val url1 = "http://foo.com"
    val url2 = "http://bar.com"
    var tokens = Seq[String]()
    within(500 millis) {
      shortUrlService ! GetShortUrlByUrl(url1)
      shortUrlService ! GetShortUrlByUrl(url2)

      receiveWhile(500 millis) {
        case msg: ShortUrl => tokens = msg.token +: tokens
      }
    }
    tokens.length shouldBe 2
    tokens.head should not equal tokens.last
  }

  it should "provide same token from same url" in {
    val url1 = "http://foo.com"
    var tokens = Seq[String]()
    within(500 millis) {
      shortUrlService ! GetShortUrlByUrl(url1)
      shortUrlService ! GetShortUrlByUrl(url1)

      receiveWhile(500 millis) {
        case msg: ShortUrl => tokens = msg.token +: tokens
      }
    }
    tokens.length shouldBe 2
    tokens.head shouldBe tokens.last
  }

  it should "provide original url by token" in {
    import akka.pattern.pipe
    import akka.pattern.ask
    import system.dispatcher

    val url = UUID.randomUUID().toString
    implicit val timeout = Timeout(1 seconds)
    (shortUrlService ? GetShortUrlByUrl(url))
      .mapTo[ShortUrl]
      .map(v => {
        println(v)
        GetShortUrlByToken(v.token)
      })
        .pipeTo(shortUrlService)

    expectMsgPF(1 seconds) {
      case ShortUrl(_, original, token) => url == original
    }
  }

  it should "provide different token from different url (heavy)" in {
    var tokens = Seq[String]()
    within(1 second) {
      for (i <- 1 to 1000) {
        shortUrlService ! GetShortUrlByUrl(UUID.randomUUID().toString)
      }
      shortUrlService ! GetShortUrlByUrl("http://hello")
      shortUrlService ! GetShortUrlByUrl("http://hello") // should got same token
      receiveWhile(1 second) {
        case msg: ShortUrl => tokens = msg.token +: tokens
      }
    }
    println(tokens)
    tokens.length shouldBe 1002
    tokens.toSet.size shouldBe 1001
  }
}
