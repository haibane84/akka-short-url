package com.jeado.shorurl

import akka.actor.{Actor, ActorLogging, Props}
import org.pico.hashids.Hashids

import scala.collection.mutable

object ShortUrlServiceActor {
  case class GetShortUrlByToken(token: String)
  case class GetShortUrlByUrl(url: String)

  case class ShortUrl(id: Long, url: String, token: String)
  object NotFoundUrl

  def props: Props = Props[ShortUrlServiceActor]
}

class ShortUrlServiceActor extends Actor with ActorLogging {
  import ShortUrlServiceActor._
  var id = 0L
  val hashids: Hashids = Hashids.reference("abc")
  var hashToUrlMap: mutable.Map[String, String] = mutable.Map[String, String]()
  var urlToIdmap: mutable.Map[String, Long] = mutable.Map[String, Long]()

  def receive: Receive = {
    case GetShortUrlByToken(token) =>
      log.debug(s"Got $token try to find")
      hashToUrlMap.get(token) match {
        case Some(url) =>
          log.debug(s"Found $url")
          sender() ! ShortUrl(hashids.decode(token).head, url, token)
        case None =>
          sender() ! NotFoundUrl
      }

    case GetShortUrlByUrl(url) =>
      log.debug(s"Try to find url $url")
      urlToIdmap.get(url) match {
        case Some(id) =>
          log.info(s"Got Id $id")
          sender() ! ShortUrl(id, url, hashids.encode(id))
        case None =>
          id += 1
          val hash = hashids.encode(id)
          hashToUrlMap += (hash -> url)
          urlToIdmap += (url -> id)

          log.info(s"New token created $id, $url, $hash")

          sender() ! ShortUrl(id, url, hash)
      }
  }
}
