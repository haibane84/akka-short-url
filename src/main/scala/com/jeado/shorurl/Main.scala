package com.jeado.shorurl

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer

import scala.concurrent.{ExecutionContext, Future}

object Main extends App with ShortUrlV1Routes {

  implicit val system: ActorSystem = ActorSystem("shortUrlService")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val executionContext: ExecutionContext = system.dispatcher

  val shortUrlServiceActor = system.actorOf(ShortUrlServiceActor.props)

  val serverBindingFuture: Future[ServerBinding] = Http().bindAndHandle(routes, "0.0.0.0", 8080)

  println(s"Server online at http://0.0.0.0:8080")
}
