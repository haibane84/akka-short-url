package com.jeado.shorurl

import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.HttpHeader._
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import play.api.libs.json.Json
import akka.pattern._
import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits._

import scala.concurrent.duration._

case class CreateShortUrl(url: String)
object CreateShortUrl {
  implicit val createShortUrlFormat = Json.format[CreateShortUrl]
}

trait ShortUrlV1Routes {
  import ShortUrlServiceActor._
  import PlayJsonSupport._

  implicit def system: ActorSystem

  implicit val shortUrlFormat = Json.format[ShortUrl]

  implicit val timeout = Timeout(3 seconds)

  lazy val log = Logging(system, classOf[ShortUrlV1Routes])

  def shortUrlServiceActor: ActorRef

  lazy val routes: Route =
    concat(
      getFromResourceDirectory("public"),
      (pathEnd | pathSingleSlash) {
        redirect("index.html", PermanentRedirect)
      },
      pathPrefix("v1" / "shorturl") {
        concat(
          (pathEnd | pathSingleSlash) {
            post {
              entity(as[CreateShortUrl]) { entity =>
                complete {
                  (shortUrlServiceActor ? GetShortUrlByUrl(entity.url)).mapTo[ShortUrl]
                }
              }
            }
          },
          path(Remaining) { token =>
            complete {
              (shortUrlServiceActor ? GetShortUrlByToken(token)).mapTo[ShortUrl]
            }
          }
        )
      },
      pathPrefix(Remaining) { token =>
        complete {
          log.info(token)
          (shortUrlServiceActor ? GetShortUrlByToken(token)).map {
            case ShortUrl(_, uri, _) =>
              HttpResponse(PermanentRedirect, headers = Location(uri) :: Nil)
            case NotFoundUrl =>
              HttpResponse(NotFound)
          }
        }
      }
    )
}
