$(() => {
    const tokenList = $('#token-list');
    const url = $('#original-url');
    const button = $("#submit");

    button.click(e => {
        e.preventDefault();
        $.ajax('v1/shorturl', {
            data: JSON.stringify({ url: url.val() }),
            contentType: 'application/json',
            type: 'POST'
        }).then(v => {
            tokenList.append($(`<li class="list-group-item"><a href="${v.token}">${v.token}</a></li>`));
        });
    });
});
