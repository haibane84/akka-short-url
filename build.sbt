lazy val akkaHttpVersion = "10.0.9"
lazy val akkaVersion    = "2.5.3"
lazy val palyVersion    = "2.6.3"
lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "com.jeado",
      scalaVersion    := "2.12.3"
    )),
    name := "akka-short-url",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,

      "com.typesafe.play" %% "play-json"            % palyVersion,
      "de.heikoseeberger" %% "akka-http-play-json"  % "1.18.0",
      "org.picoworks"     %% "pico-hashids"         % "4.4.141",

      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
      "org.scalatest"     %% "scalatest"         % "3.0.1"         % Test
    )
  )
